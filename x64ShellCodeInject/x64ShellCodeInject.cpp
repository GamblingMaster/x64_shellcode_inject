// x64ShellCodeInject.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
void inject_x64(DWORD ProcessId, LPCTSTR dll_file);

int _tmain(int argc,TCHAR *argv[])
{
	if (argc!=3)
	{
		_tprintf(_T("USAGE: <pid> <dll_to_inject>\n"));
		exit(0);
	}
	auto pid = _ttol(argv[1]);
	auto dll_file = argv[2];
	inject_x64(pid, dll_file);
    return 0;
}

