// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"

#include<thread>
VOID threadtest1()
{
	//auto thread = std::thread([&]() {
	//	MessageBoxA(nullptr, "hello2", "ok", MB_OK);
	//});
	//thread.join();
}
DWORD WINAPI threadTest2(LPVOID param)
{
	threadtest1();
	MessageBoxA(nullptr, "hello", "ok", MB_OK);
	return 0;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		//MessageBoxA(nullptr, "hello", "ok", MB_OK);
		CreateThread(nullptr, 0, threadTest2, nullptr, 0, nullptr);
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

